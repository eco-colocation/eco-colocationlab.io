import React, { useState } from "react"
import Axios from "axios"

export default props => {

    const [url, setUrl] = useState("")
    const [name, setName] = useState("")


    const handleOnUrlChange = (event) => {
        setUrl(event.target.value)
    }
    const handleOnNameChange = (event) => {
        setName(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();

        props.setVisios({
            ...props.visios,
            [Object.keys(props.visios).length + 1]: 
                {
                    url,
                    name
                }
            }
        )

        Axios.post("http://51.178.183.220:3000/visios/", 
            {
                ...props.visios,
            [Object.keys(props.visios).length + 1]: 
                {
                    url,
                    name
                }
            }
        ).then().catch((error) => console.log(error))
    }

    return (
        <div>
            <form>
                <label>
                    Nom de la Room
                </label>
                <input name="name" placeholder="Nom à donner à la room" style={{ width: "30%", paddingLeft: "2%" }}
                onChange={handleOnNameChange}
                >
                </input>
                <br />
                <label>
                    Adresse de la room
                </label>
                <input name="url" placeholder="URL vers une room" style={{ width: "30%", paddingLeft: "2%" }}
                onChange={handleOnUrlChange}
                >
                </input>
                <br />
                <button type="submit" onClick={handleSubmit}>Ajouter la room</button>
            </form>
        </div>
    )
}