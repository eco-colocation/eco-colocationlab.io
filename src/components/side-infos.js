import React, { useState } from "react"

export default props => {

    const initialDisplayInfos = true

    const [displayInfos, setDisplayInfos] = useState(initialDisplayInfos)

    const handleHideInfos = event => {
        setDisplayInfos(
            displayInfos === true
            ? false
            : true
        )
    }

    return (
        <div style={{
            position: "absolute",
            top: "25%",
            right: "4%",
            width:"20%"
        }}>
            {
                displayInfos === true
                ?
                <div>
                    <p>
                        Bienvenue ! Dans le canal accueil, vous serez accueillie, ça va être bref, c'est juste pour vous donner les infos !
                    </p>
                    <p>
                        Pour quitter un salon et passer à un autre, il vous suffit de cliquer sur le nom du salon.
                    </p>
                    <p>
                        Le canal plénière est celui où chacun·e est invité·e à discuter tou·te·s ensemble. Veillez bien à y demander la parole et à attendre qu'on vous la donne !
                    </p>
                    <p>
                        La parole sera plus libre dans les canaux par région et par atelier !
                    </p>
                    <button onClick={handleHideInfos}>
                        Cacher ces infos
                    </button>
                </div>
                :
                <button onClick={handleHideInfos}>
                    Montrer les infos
                </button>
            }
        </div>
    )
}