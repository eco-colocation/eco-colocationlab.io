import React from "react"

export default () => {

    return(
        <div>
            <table><tbody><tr>
                <td>
                    <a href="/"><button>Home</button></a>
                </td>
                <td>
                    <a href="/contribuer"><button>Contribuer</button></a>
                </td>
                <td>
                    <a href="/visios"><button>Visios</button></a>
                </td>
                <td>
                    <a href="/admin"><button>Admin</button></a>
                </td>
            </tr></tbody></table>
        </div>
    )
}