import React from "react"
import Navigation from "../components/navigation"

export default () => {

    return(
        <div style={{padding: "0 20% 0 20%"}}>
            <Navigation />
            
            <h3>Assos organisant des ateliers modes de vie écolos</h3>
            <iframe title="airtable-tableur" class="airtable-embed" src="https://airtable.com/embed/shrnrBhiIq0oGegOU?backgroundColor=green&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" ></iframe>
            <p>Ajouter une organisation</p>
            <script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script><iframe class="airtable-embed airtable-dynamic-height" src="https://airtable.com/embed/shry0vSMSlXzVqB2s?backgroundColor=green" frameborder="0" onmousewheel="" width="100%" height="611.166667"></iframe>
         </div>
    )
}