import React from "react"
import Navigation from "../components/navigation"

export default () => {

    return(
        <div>
            <Navigation />
            

            <div>
                <table><tbody><tr>
                    <td>
                        <iframe title="carte" src="https://eco-colocation.gogocarto.fr/annuaire#/carte/@46.33,2.50,6z?cat=all" width="100%" height="533px"></iframe>
                    </td>
                    <td>
                        <iframe id="open-web-calendar"
                            title="agenda"
                            style={{background:"url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat"}}
                            src="https://open-web-calendar.herokuapp.com/calendar.html?url=https%3A%2F%2Fairtable.com%2FshrM8lNyMeuBhWKUm%2FiCal%3FtimeZone%3DEurope%252FParis%26userLocale%3Den"
                            sandbox="allow-scripts allow-same-origin allow-top-navigation"
                            allowTransparency="true" scrolling="no" 
                            frameborder="0" height="600px" width="100%"></iframe>
                    </td>
                </tr></tbody></table>
            </div>
            <div>
                <p>Posts qui circulent sur fb</p>
                <iframe title="rss-fb" width="100%" height="533px" src="https://rss-bridge.sans-nuage.fr/?action=display&bridge=Facebook&context=Group&g=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F470511086490229%2F&limit=-1&format=Html"></iframe>
            </div>
        </div>


    )
}