import React from "react"
import Navigation from "../components/navigation"

export default () => {

    const js_code_extract_missions_from_modules = `
    //window.location= "https://www.facebook.com/groups/470511086490229/" + "learning_content/"

missions = {}
thread = document.getElementById("pagelet_group_learning_content")
blocks = thread.querySelectorAll("._4-u2._4-u8")


i=0
for (block of blocks){
	if (i>2){
		console.log(i)
	}
	title = block.querySelectorAll("._6a._5u5j._6b")[0].querySelector("a").innerHTML
	missions[title] = {"title": title, "missions": []}

	for (item of block.querySelectorAll("._8u._42ef") ){
		missions[title]["missions"].push(item.innerHTML)
	}
	i = i+1
}`


    return(
        <div>
            <Navigation />
            <iframe title="contribuer.eco-colocs.fr" src="https://contribuer.eco-colocations.gitlab.io"></iframe>
            
            <div>
                {js_code_extract_missions_from_modules}
            </div>
        </div>
    )
}