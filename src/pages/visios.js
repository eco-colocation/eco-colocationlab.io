import React, { useState } from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import ConfigRooms from "../components/config-rooms"
import SideInfos from "../components/side-infos"

import Axios from "axios"

import Navigation from "../components/navigation"

const IndexPage = () => {

  const initialNoRoom = {"0":{url: "https://c-6828.ensemble.scaleway.com/nrByKriOq", name: "Dépannage et aide"}}

  const [visios,setVisios] = useState(initialNoRoom)

  const updateRooms = () => {
    Axios.get(
      "http://51.178.183.220:3000/visios/",
    ).then((response) => 
      {
        setVisios(response.data)
      }).catch((error)=> console.log(error))
  }

  if ( visios === initialNoRoom ) updateRooms()

  const [displayRoomsConfig, setDisplayRoomsConfig] = useState(false)
  const handleConfigRoomsClick = () => {
    setDisplayRoomsConfig(
      displayRoomsConfig === true ? false : true
    )
  }

  const [ selectedRoom, setSelectedRoom ] = useState(0)

  const handleChangeRoom = (event) => {
    setSelectedRoom(parseInt(event.target.value))
  }

  return(
    <Layout>
      <SEO title="Home" />
      <Navigation />
      <div>
        <p>Un apéro comme celui-là, tu n'en vivras pas d'autres !</p>
        <p>
          Coupez votre micro.
        </p>
        <p>
          Posez vos questions dans le chat. Il y a deux animateur·ices pour les lire, et qui vont les passer aux orateur·ices.
        </p>
        <p>
          En cas de soucis, cliquez sur le bouton "Dépannage et aide". En cas d'impossibilité à joindre l'appel, écrivez un MP à <a href="facebook.com/jibe.b.api/">Jibé</a>
        </p>
        <div>
          <p>Salon actuel : {visios[selectedRoom].name}</p>
          <a href={visios[selectedRoom].url}>Accès direct au salon</a>
        </div>
        {
          Object.keys(visios).map(
            key =>
              <button value={key} onClick={handleChangeRoom}>
                {visios[key].name}
              </button>
          )
        }

        <iframe title="room" src={visios[selectedRoom].url} 
        allow="camera;microphone"
        width="100%" height="600px" />
      </div>

      <button onClick={updateRooms}>
          Mettre à jour les rooms
      </button>

      <button onClick={handleConfigRoomsClick}>
        Configurer les rooms
      </button>
      {
        displayRoomsConfig === true
        ? <ConfigRooms updateRooms={updateRooms} visios={visios} setVisios={setVisios}/>
        : null
      }

      <SideInfos />
    </Layout>
  )
}
export default IndexPage
