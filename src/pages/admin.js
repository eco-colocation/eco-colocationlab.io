import React from "react"
import Navigation from "../components/navigation"

export default () => {

    return(
        <div>
            <Navigation />
            <iframe title="airtable" class="airtable-embed" src="https://airtable.com/embed/shrgMhYoKQN0oKAmv?backgroundColor=green&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style={{background: "transparent", border: "1px solid #ccc"}}></iframe>
            <iframe title="gogocarto-admin" src="https://eco-colocation.gogocarto.fr/admin/dashboard"  width="100%" height="533" ></iframe>
            <a href="https://www.facebook.com/groups/472175417056699/">Groupe des admins</a>
        </div>
    )
}